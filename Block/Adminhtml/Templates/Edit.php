<?php
namespace Ramji\PushNotification\Block\Adminhtml\Templates;

/**
 * CMS block edit form container
 */
class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'Ramji_PushNotification';
        $this->_controller = 'adminhtml_templates';

        parent::_construct();

        $this->buttonList->update('save', 'label', __('Save Template'));
        $this->buttonList->update('delete', 'label', __('Delete Template'));

        $this->buttonList->add(
            'saveandcontinue',
            array(
                'label' => __('Save and Continue Edit'),
                'class' => 'save',
                'data_attribute' => array(
                    'mage-init' => array('button' => array('event' => 'saveAndContinueEdit', 'target' => '#edit_form'))
                )
            ),
            -100
        );
    }

    /**
     * Get edit form container header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        if ($this->_coreRegistry->registry('pushnotification_template')->getId()) {
            return __("Edit Template '%1'", $this->escapeHtml($this->_coreRegistry->registry('pushnotification_template')->getTitle()));
        } else {
            return __('Create Template');
        }
    }
}
