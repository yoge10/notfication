<?php
namespace Ramji\PushNotification\Block\Adminhtml\Templates\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    protected function _construct()
    {
        parent::_construct();
        $this->setId('pushnotification_templates');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Template Information'));
    }
}