<?php
namespace Ramji\PushNotification\Block\Adminhtml;
class Templates extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_templates';/*block grid.php directory*/
        $this->_blockGroup = 'Ramji_PushNotification';
        $this->_headerText = __('Templates Information');
        $this->_addButtonLabel = __('Add Template'); 
        parent::_construct();
    }
}
