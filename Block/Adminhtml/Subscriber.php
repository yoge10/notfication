<?php
namespace Ramji\PushNotification\Block\Adminhtml;
class Subscriber extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_subscriber';/*block grid.php directory*/
        $this->_blockGroup = 'Ramji_PushNotification';
        $this->_headerText = __('Subscribers');
        $this->_addButtonLabel = __('Add New Subscriber'); 
        parent::_construct();
    }
}
