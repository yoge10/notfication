<?php
/**
 * Copyright © 2015 Ramji. All rights reserved.
 */
namespace Ramji\PushNotification\Model\ResourceModel;

/**
 * Notification resource
 */
class Notification extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('pushnotification_notification', 'id');
    } 
}
