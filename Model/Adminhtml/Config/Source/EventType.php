<?php
namespace Ramji\PushNotification\Model\Adminhtml\Config\Source;
 
class EventType implements \Magento\Framework\Option\ArrayInterface
{
    const ORDERHOLD = 1;
    const ORDERPROCESS = 2;
    const ORDERSHIPED = 3;
    const ORDERCANCEL = 4;
    const ORDERCREDIT = 5;
    const CUSTOMERLOGIN = 6;
    const CUSTOMERLOGOUT = 7;
    const CUSTOMERSIGNUP = 8;
    const NEWSLETTERSUBSCRIBE = 9;
    const NEWSLETTERUNSUBSCRIBE = 10;
    const CUSTOMERBIRTHDAY = 11;
    const ADMINLOGIN = 12;
    const ADMINLOGINFAIL = 13;
    const REVIEW = 14;
    const WHISTLIST = 15;
    
    public function toOptionArray()
    {
        return [['value' => NULL, 'label' => __('-- Select Page --')],
                ['value' => NULL, 'label' => __('-- ORDER STATUS --')],
                ['value' => self:: ORDERHOLD, 'label' => __('Order Hold')],
                ['value' => self:: ORDERPROCESS, 'label' => __('Order Process')], 
                ['value' => self:: ORDERSHIPED, 'label' => __('Order Shipped')],
                ['value' => self:: ORDERCANCEL, 'label' => __('Order Cancel')],
                ['value' => self:: ORDERCREDIT, 'label' => __('Order Credit')],
                ['value' => NULL, 'label' => __('----- CUSTOMER LOGIN -----')],
                ['value' => self:: CUSTOMERLOGIN, 'label' => __('Customer Login')],
                ['value' => self:: CUSTOMERLOGOUT, 'label' => __('Customer Logout')],
                ['value' => self:: CUSTOMERSIGNUP, 'label' => __('Customer SingUp')],
                ['value' => self:: CUSTOMERBIRTHDAY, 'label' => __('Customer BirthDay')],
                ['value' => NULL, 'label' => __('---- NEWSLETTER SUBSCRIPTION/UNSUBSCRIPTION ----')],
                ['value' => self:: NEWSLETTERSUBSCRIBE, 'label' => __('Newsletter Subscribe')],
                ['value' => self:: NEWSLETTERUNSUBSCRIBE, 'label' => __('Newsletter Unsubscribe')],
                ['value' => NULL, 'label' => __('-- ADMIN LOGIN --')],
                ['value' => self:: ADMINLOGIN, 'label' => __('Admin Login')],
                ['value' => self:: ADMINLOGINFAIL, 'label' => __('Admin Login Fails')],
                ['value' => NULL, 'label' => __('-- USER REVIEW --')],
                ['value' => self:: REVIEW, 'label' => __('Review')],
                ['value' => NULL, 'label' => __('-- WHISLIST --')],
                ['value' => self:: WHISTLIST, 'label' => __('Whislist Add Product')],
                ];
    }   
}