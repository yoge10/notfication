<?php
namespace Ramji\PushNotification\Model\Adminhtml\Config\Source;
 
class ScheduleEvent implements \Magento\Framework\Option\ArrayInterface
{
    const MINUTES_15 = 15;
    const MINUTES_30 = 30;
    const HOUR_1 = 60;
    const HOUR_2 = 120;
    const DAY_1 = 1440;
    const CUSTOM = 6;
    
    public function toOptionArray()
    {
        return [['value' => NULL, 'label' => __('-- Select Page --')],
                ['value' => self:: MINUTES_15, 'label' => __('15 Minutes')], 
                ['value' => self:: MINUTES_30, 'label' => __('30 Minutes')],
                ['value' => self:: HOUR_1, 'label' => __('1 Hour')], 
                ['value' => self:: HOUR_2, 'label' => __('2 Hours')],
                ['value' => self:: DAY_1, 'label' => __('1 Day')], 
                ['value' => self:: CUSTOM, 'label' => __('Custom')],
                ];            
    }
}