<?php
namespace Ramji\PushNotification\Model\Adminhtml\Config\Source;
 
class TriggerType implements \Magento\Framework\Option\ArrayInterface
{
    const MANUAL = 1;
    const TRIGGERED = 2;
    const EVENT = 3;
    
    public function toOptionArray()
    {
        return [['value' => NULL, 'label' => __('-- Select Page --')],
                ['value' => self:: MANUAL, 'label' => __('Manual Notification')], 
                ['value' => self:: TRIGGERED, 'label' => __('Triggered Notification')],
                ['value' => self:: EVENT, 'label' => __('Event Notification')]
                ];            
    }   
    public function optionArray()
    {
        return [
                self:: MANUAL => __('Manual Notification'), 
                self:: TRIGGERED => __('Triggered Notification'),
                self:: EVENT => __('Event Notification'),
            ];            
    } 
}