<?php
namespace Ramji\PushNotification\Model\Adminhtml\Config\Source;
 
class Template implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var \Ramji\PushNotification\Model\ResourceModel\Templates\Collection $collectionFactory
     */
    protected $_collectionFactory;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Ramji\PushNotification\Model\ResourceModel\Templates\Collection $collectionFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        array $data = []
    ) {
        $this->_collectionFactory = $collectionFactory;
        $this->_messageManager = $messageManager;
    }
    public function toOptionArray()
    {
        try{
            $collection =$this->_collectionFactory->load();
            $templateCollection = [];
            if(!empty($collection)){
                foreach ($collection as $templateName) {
                    $templateCollection[] = ['value' => $templateName['id'], 'label' => __($templateName['name'])];  
                }
            }else{
                $templateCollection[] = ['label' => __('Please Add Template First')];
            }
        }catch(Exception $e){
            $this->_messageManager->addError(__("Something went wrong."));
        }
        return $templateCollection;      
    }   
    public function optionArray()
    {
        $collection =$this->_collectionFactory->load();
        foreach ($collection as $templateName) {
            $templateCollection[$templateName['id']] = __($templateName['name']);
        }
        return $templateCollection; 
    }   
}