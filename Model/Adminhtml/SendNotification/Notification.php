<?php
namespace Ramji\PushNotification\Model\Adminhtml\SendNotification;
 
class Notification
{
    protected $_helperData;
    
    //for Token
    protected $_subscribe;
    protected $_storeManagaer;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManagaer,
        \Ramji\PushNotification\Model\SubscriberFactory $subscribe,
        \Ramji\PushNotification\Helper\Data $helperData,
        array $data = []
    ) {
        $this->_storeManagaer = $storeManagaer;
        $this->_subscribe = $subscribe;
        $this->_helperData = $helperData;
    }
    public function sendNotification($templateInfo,$notificationId)
    {
        $subscribeData = $this->_subscribe->create()->getCollection();
        $baseUrl = $this->_storeManagaer->getStore()->getBaseUrl();
        $notificationEnable = $this->_helperData->getGeneralConfig('enable');
        $apiKey = $this->_helperData->getGeneralConfig('api_key');
        $apiUrl = $this->_helperData->getGeneralConfig('url');
        if(!empty($subscribeData->getData()) && $notificationEnable == '1'){
            foreach ($subscribeData as $subscription) {
                $status = $subscription->getStatus();
                $storeView = $subscription->getStoreView();
                $storeViewInfo = $this->_helperData->getStoreViewValidate($storeView,$notificationId);
                if(($status == '1')&& ($storeViewInfo == '1')){
                    $url = $apiUrl;
                    $registrationIds = $subscription->getToken();
                    $token= $registrationIds;
                    $title = $templateInfo['subject'];
                    $message = $templateInfo['messageBody'];
                    $templateUrl = $baseUrl.$templateInfo['url'];
                    $messageIcon = $this->_helperData->getMediaUrl().$templateInfo['icon'];
                    $token = htmlspecialchars($token,ENT_COMPAT);
                    $title = htmlspecialchars($title,ENT_COMPAT);
                    $message = htmlspecialchars($message,ENT_COMPAT);
                    // Push Data's
                    $data = array(
                        "to" => "$token",
                        "data" => array( 
                            "title" => "$title", 
                            "body" => "$message", 
                            "icon" => "$messageIcon",
                            "click_action"=> "$templateUrl"
                        )
                    );
                    // Print Output in JSON Format
                    $data_string = json_encode($data);
                    // FCM API Token URL
                    $url = $apiUrl;
                    //Curl Headers
                    $headers = array
                    (
                        'Authorization: key=' . $apiKey, 
                        'Content-Type: application/json'
                    );
                    $ch = curl_init();  
                    curl_setopt($ch, CURLOPT_URL, $url);                                                                 
                    curl_setopt($ch, CURLOPT_POST, 1);  
                    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                    curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string);                                                                                                                                                                      
                    // Variable for Print the Result
                    $result = curl_exec($ch);
                    curl_close ($ch);
                }
            }
            return true;
        }else{
            return false;
        }
    }
    public function sendOrderNotification($templateInfo,$token){
        $baseUrl = $this->_storeManagaer->getStore()->getBaseUrl();
        $notificationEnable = $this->_helperData->getGeneralConfig('enable');
        $apiKey = $this->_helperData->getGeneralConfig('api_key');
        $apiUrl = $this->_helperData->getGeneralConfig('url');
        if(!empty($token)){
            $url = $apiUrl;
            $token= $token;
            $title = $templateInfo['subject'];
            $message = $templateInfo['messageBody'];
            $templateUrl = $baseUrl.$templateInfo['url'];
            $messageIcon = $this->_helperData->getMediaUrl().$templateInfo['icon'];
            $token = htmlspecialchars($token,ENT_COMPAT);
            $title = htmlspecialchars($title,ENT_COMPAT);
            $message = htmlspecialchars($message,ENT_COMPAT);
            // Push Data's
            $data = array(
                "to" => "$token",
                "notification" => array( 
                    "title" => "$title", 
                    "body" => "$message", 
                    "icon" => "$messageIcon",
                    "click_action"=> "$templateUrl"
                )
            );
            // Print Output in JSON Format
            $data_string = json_encode($data);
            // FCM API Token URL
            $url = $apiUrl;
            //Curl Headers
            $headers = array
            (
                'Authorization: key=' . $apiKey, 
                'Content-Type: application/json'
            );
            $ch = curl_init();  
            curl_setopt($ch, CURLOPT_URL, $url);                                                                 
            curl_setopt($ch, CURLOPT_POST, 1);  
            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string);                                                                                                                                                                      
            // Variable for Print the Result
            $result = curl_exec($ch);
            curl_close ($ch);
        }
    }
}