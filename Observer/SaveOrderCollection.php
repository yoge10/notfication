<?php

namespace Ramji\PushNotification\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class SaveOrderCollection implements ObserverInterface
{
    // /**
    //  * @var \Magento\Checkout\Model\Session
    //  */
    // const ORDER_PLACE_AMOUNT = 'storecredit/admin_storecredit_Setting/order_place_credit';
    // const MODULE_STATUS = 'storecredit/advanced_setting/enable_control';

    protected $request;
    protected $_cookies;
    protected $_customOrderCollection;
    protected $_helperData;
    /**
     * AddFeeToOrderObserver constructor.
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookie,
        \Ramji\PushNotification\Helper\Data $helperData,
        \Ramji\PushNotification\Model\OrderCollectionFactory $customOrderCollection
    )
    {
        $this->request = $request;
        $this->_cookies = $cookie;
        $this->_helperData = $helperData;
        $this->_customOrderCollection = $customOrderCollection;
    }

    /**
     * set customer credit data
     *
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $this->_customOrderCollection->create();
        $event = $observer->getEvent();
        $orderIds = $event->getOrder();
        $orderIncrementId = $orderIds->getIncrementId();
        $customerEmail = $orderIds->getCustomerEmail();
        $token = $this->_cookies->getCookie('token');
        $storeView = $this->_helperData->getStoreId();
        $data['store_view'] = $storeView;
        $data['token'] = $token;
        $data['order_increment_id'] = $orderIncrementId;
        $data['customer_email'] = $customerEmail;
        $order->setData($data);
        $order->save();
    }
}