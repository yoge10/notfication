<?php
namespace Ramji\PushNotification\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class AdminLoginFails implements ObserverInterface
{
    protected $dataHelper;
    protected $_sendNotification;
    protected $_cookiesData;
    public function __construct(
        \Ramji\PushNotification\Helper\Data $dataHelper,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookie,
        \Ramji\PushNotification\Model\Adminhtml\SendNotification\Notification $sendNotification
    )
    {
        $this->dataHelper = $dataHelper;
        $this->_cookiesData = $cookie;
        $this->_sendNotification = $sendNotification;
    }
 
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $triggerType = 3;
        $adminLoginFails = 13;
        $token = $this->_cookiesData->getCookie('token');
        $customer = $observer->getEvent();
        $customerName = $customer->getUserName();
        $storeView = $this->dataHelper->getStoreId();
        $triggerTypeCollection = $this->dataHelper->getOrderStatusChange($triggerType,$adminLoginFails);
        $storeViewValidation = $this->dataHelper->getStoreViewValidate($storeView,$triggerTypeCollection);
        if(($triggerTypeCollection != 0) && ($storeViewValidation == '1')){
            $getTemplateCollection = $this->dataHelper->getTemplate($triggerTypeCollection);
            $updatedTemplate = $this->dataHelper->getCustomerTemplateUpdate($getTemplateCollection,$customerName);
            $this->_sendNotification->sendOrderNotification($updatedTemplate,$token);
        }
    }
}