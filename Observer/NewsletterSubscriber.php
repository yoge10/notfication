<?php
namespace Ramji\PushNotification\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class NewsletterSubscriber implements ObserverInterface
{
    protected $dataHelper;
    protected $_sendNotification;
    protected  $_customer;
    protected $_cookies;
    public function __construct(
        \Ramji\PushNotification\Helper\Data $dataHelper,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookie,
        \Ramji\PushNotification\Model\Adminhtml\SendNotification\Notification $sendNotification,
        \Magento\Customer\Model\Customer $customer
    )
    {
        $this->_customer = $customer;
        $this->_cookies = $cookie;
        $this->dataHelper = $dataHelper;
        $this->_sendNotification = $sendNotification;
    }
 
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        // Event
        $triggerType = 3;
        $newsletterSubscribe = 9;
        $newsletterUnsubscribe = 10;

        $event = $observer->getEvent();
        $newsletter = $event->getSubscriber();
        $email = $newsletter->getsubScriberEmail();
        $status = $newsletter->getSubscriberStatus();
        $customerId = $newsletter->getCustomerId();
        $customer = $this->_customer->getCollection()->addAttributeToFilter('entity_id',$customerId);
        $customerCollection = $customer->getData();
        $firstName = $customerCollection[0]['firstname'];
        $lastName = $customerCollection[0]['lastname'];
        $customerName = $firstName.' '.$lastName;
        $token = $this->_cookies->getCookie('token');
        $storeView = $this->dataHelper->getStoreId();
        if($status == 3){
            $triggerTypeCollection = $this->dataHelper->getOrderStatusChange($triggerType,$newsletterUnsubscribe);
            $storeViewValidation = $this->dataHelper->getStoreViewValidate($storeView,$triggerTypeCollection);
            if(($triggerTypeCollection != 0) && ($storeViewValidation == '1')){
                $getTemplateCollection = $this->dataHelper->getTemplate($triggerTypeCollection);
                $updatedTemplate = $this->dataHelper->getCustomerTemplateUpdate($getTemplateCollection,$customerName);
                $this->_sendNotification->sendOrderNotification($updatedTemplate,$token);
            }
        }elseif($status == 1){
            $triggerTypeCollection = $this->dataHelper->getOrderStatusChange($triggerType,$newsletterSubscribe);
            $storeViewValidation = $this->dataHelper->getStoreViewValidate($storeView,$triggerTypeCollection);
            if(($triggerTypeCollection != 0) && ($storeViewValidation == '1')){
                $getTemplateCollection = $this->dataHelper->getTemplate($triggerTypeCollection);
                $updatedTemplate = $this->dataHelper->getCustomerTemplateUpdate($getTemplateCollection,$customerName);
                $this->_sendNotification->sendOrderNotification($updatedTemplate,$token);
            }

        }
    }
}