<?php
namespace Ramji\PushNotification\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class CustomerNotification implements ObserverInterface
{
    protected $dataHelper;
    protected $_sendNotification;
    public function __construct(
        \Ramji\PushNotification\Helper\Data $dataHelper,
        \Ramji\PushNotification\Model\Adminhtml\SendNotification\Notification $sendNotification
    )
    {
        $this->dataHelper = $dataHelper;
        $this->_sendNotification = $sendNotification;
    }
 
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        //Event
        $triggerType = 3;

        //Order Status
        $orderOnHold = 1;
        $orderProcess = 2;
        $orderShipped = 3;
        $orderCancel = 4;
        $orderCredited = 5;

        $order = $observer->getEvent()->getOrder();
        $orderStatus = $order->getStatus();
        $statusLabel = $order->getStatusLabel();
        $orderIncrementId = $order->getIncrementId();
        $orderId = $order->getId();
        $customerEmail = $order->getCustomerEmail();
        $state = $order->getState();
        $customOrderData = $this->dataHelper->getCustomToken($orderId,$customerEmail);
        $customerName = $order->getCustomerName();
        $token = $customOrderData->getData()[0]['token'];
        $storeView = $customOrderData->getData()[0]['store_view'];
        $pushnotitficationEnable = $this->dataHelper->getTokenConfig('enable');
        $triggerTypeCollection = 0;
        if($pushnotitficationEnable == '1'){
            if($orderStatus == 'holded'){
                $triggerTypeCollection = $this->dataHelper->getOrderStatusChange($triggerType,$orderOnHold);
            }elseif($orderStatus == 'processing'){
                $triggerTypeCollection = $this->dataHelper->getOrderStatusChange($triggerType,$orderProcess);
            }elseif($orderStatus == 'complete'){
                $triggerTypeCollection = $this->dataHelper->getOrderStatusChange($triggerType,$orderShipped);
            }elseif($orderStatus == 'canceled'){
                $triggerTypeCollection = $this->dataHelper->getOrderStatusChange($triggerType,$orderCancel);
            }elseif($orderStatus == 'closed'){
                $triggerTypeCollection = $this->dataHelper->getOrderStatusChange($triggerType,$orderCredited);
            }
            
            if($triggerTypeCollection != 0){
                $getTemplateCollection = $this->dataHelper->getTemplate($triggerTypeCollection);
                $storeViewValidation = $this->dataHelper->getStoreViewValidate($storeView,$triggerTypeCollection);
                if($storeViewValidation == '1'){
                    $updatedTemplate = $this->dataHelper->getTemplateUpdate($getTemplateCollection,$orderIncrementId,$orderId,$statusLabel,$customerName);
                    $this->_sendNotification->sendOrderNotification($updatedTemplate,$token);
                }
            }
        }
    }
}