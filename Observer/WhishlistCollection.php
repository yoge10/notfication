<?php
namespace Ramji\PushNotification\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class WhishlistCollection implements ObserverInterface
{
    protected $dataHelper;
    protected $_sendNotification;
    protected $_cookiesData;
    protected $_customerSession;
    public function __construct(
        \Ramji\PushNotification\Helper\Data $dataHelper,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookie,
        \Magento\Customer\Model\Session $customerSession,
        \Ramji\PushNotification\Model\Adminhtml\SendNotification\Notification $sendNotification
    )
    {
        $this->_customerSession = $customerSession;
        $this->dataHelper = $dataHelper;
        $this->_cookiesData = $cookie;
        $this->_sendNotification = $sendNotification;
    }
 
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $triggerType = 3;
        $whislist = 15;
        $token = $this->_cookiesData->getCookie('token');
        $eventCollection = $observer->getEvent();
        $productInfo = $eventCollection->getItem();
        $customerCollection = $this->_customerSession->getCustomer();
        $firstname = $customerCollection['firstname'];
        $lastname = $customerCollection['lastname'];
        $customerName = $firstname.' '.$lastname;
        $productName = $productInfo->getName();
        $storeView = $this->dataHelper->getStoreId();
        $triggerTypeCollection = $this->dataHelper->getOrderStatusChange($triggerType,$whislist);
        $storeViewValidation = $this->dataHelper->getStoreViewValidate($storeView,$triggerTypeCollection);
        if(($triggerTypeCollection != 0) && ($storeViewValidation == '1')){
            $getTemplateCollection = $this->dataHelper->getTemplate($triggerTypeCollection);
            $updatedTemplate = $this->dataHelper->getCustomerproductUpdate($getTemplateCollection,$customerName,$productName);
            $this->_sendNotification->sendOrderNotification($updatedTemplate,$token);
        }
    }
}