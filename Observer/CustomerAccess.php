<?php
namespace Ramji\PushNotification\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class CustomerAccess implements ObserverInterface
{
    protected $dataHelper;
    protected $_sendNotification;
    protected $_cookiesData;
    public function __construct(
        \Ramji\PushNotification\Helper\Data $dataHelper,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookie,
        \Ramji\PushNotification\Model\Adminhtml\SendNotification\Notification $sendNotification
    )
    {
        $this->dataHelper = $dataHelper;
        $this->_cookiesData = $cookie;
        $this->_sendNotification = $sendNotification;
    }
 
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $triggerType = 3;
        $customerLogin = 6;
        $customerBirthDayEvent = 11;
        $token = $this->_cookiesData->getCookie('token');
        $customer = $observer->getEvent()->getCustomer();
        $customerName = $customer->getName();
        $customerBirthDay = $customer->getDob();
        $currentTime = $this->dataHelper->getTimeAccordingToTimeZone();
        $updatedCurrentTime = substr($currentTime,5,5);
        $storeView = $this->dataHelper->getStoreId();
        
        $birthDayDate = substr($customerBirthDay,5,5);
        if(!empty($updatedCurrentTime) && $updatedCurrentTime == $birthDayDate){
            $triggerTypeInfo = $this->dataHelper->getOrderStatusChange($triggerType,$customerBirthDayEvent);
            $storeViewValidation = $this->dataHelper->getStoreViewValidate($storeView,$triggerTypeInfo);
            if(($triggerTypeInfo != 0) && ($storeViewValidation == '1')){ 
                $getTemplateCollection = $this->dataHelper->getTemplate($triggerTypeInfo);
                $updatedTemplate = $this->dataHelper->getCustomerTemplateUpdate($getTemplateCollection,$customerName);
                $this->_sendNotification->sendOrderNotification($updatedTemplate,$token);
            }
        } 
        $triggerTypeCollection = $this->dataHelper->getOrderStatusChange($triggerType,$customerLogin);
        $storeViewValidation = $this->dataHelper->getStoreViewValidate($storeView,$triggerTypeCollection);
        if(($triggerTypeCollection != 0) && ($storeViewValidation == '1')){
            $getTemplateCollection = $this->dataHelper->getTemplate($triggerTypeCollection);
            $updatedTemplate = $this->dataHelper->getCustomerTemplateUpdate($getTemplateCollection,$customerName);
            $this->_sendNotification->sendOrderNotification($updatedTemplate,$token);
        }
    }
}