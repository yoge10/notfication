<?php
namespace Ramji\PushNotification\Controller\Adminhtml\Templates;
use Magento\Framework\App\Filesystem\DirectoryList;
class Save extends \Magento\Backend\App\Action
{
    /**
    * @var \Magento\Framework\Image\AdapterFactory
    */
    protected $adapterFactory;

    /**
    * @var \Magento\MediaStorage\Model\File\UploaderFactory
    */
    protected $uploader;

    /**
    * @var \Magento\Framework\Filesystem
    */
    protected $filesystem;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Image\AdapterFactory $adapterFactory,
        \Magento\MediaStorage\Model\File\UploaderFactory $uploader,
        \Magento\Framework\Filesystem $filesystem
    ) {
        $this->adapterFactory = $adapterFactory;
        $this->uploader = $uploader;
        $this->filesystem = $filesystem;
        parent::__construct($context);
    }

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();
        if ($data) {
            $model = $this->_objectManager->create('Ramji\PushNotification\Model\Templates');
            //start block upload image
            if (isset($_FILES['icon']) && isset($_FILES['icon']['name']) && strlen($_FILES['icon']['name'])) {
                /*
                * Save image upload
                */
                try {
                    $base_media_path = '';
                    $uploader = $this->uploader->create(
                        ['fileId' => 'icon']
                    );
                    $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                    $imageAdapter = $this->adapterFactory->create();
                    $uploader->addValidateCallback('icon', $imageAdapter, 'validateUploadFile');
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(true);
                    $mediaDirectory = $this->filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
                    $result = $uploader->save(
                        $mediaDirectory->getAbsolutePath()
                    );
                    $data['icon'] = $result['file'];
                } catch (\Exception $e) {
                    if ($e->getCode() == 0) {
                        $this->messageManager->addError($e->getMessage());
                    }
                }
            } else {
                if (isset($data['icon']) && isset($data['icon']['value'])) {
                    if (isset($data['icon']['delete'])) {
                        $data['icon'] = null;
                        $data['delete_image'] = true;
                    } elseif (isset($data['icon']['value'])) {
                        $data['icon'] = $data['icon']['value'];
                    } else {
                        $data['icon'] = null;
                    }
                }
            }
            
            //end block upload image
            $id = $this->getRequest()->getParam('id');
            if ($id) {
                $model->load($id);
            }
            $model->setData($data);
            try {
                $model->save();
                $this->messageManager->addSuccess(__('Template Has been Saved.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId(), '_current' => true));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (\Magento\Framework\Model\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the templates.'));
            }
            $this->_getSession()->setFormData($data);
            $this->_redirect('*/*/edit', array('templates_id' => $this->getRequest()->getParam('templates_id')));
            return;
        }
        $this->_redirect('*/*/');
    }
}
