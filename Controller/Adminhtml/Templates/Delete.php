<?php
namespace Ramji\PushNotification\Controller\Adminhtml\Templates;

class Delete extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        try {
            $templates = $this->_objectManager->get('Ramji\PushNotification\Model\Templates')->load($id);
            $templates->delete();
            $this->messageManager->addSuccess(
                __('Deleted successfully !')
            );
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }
}
