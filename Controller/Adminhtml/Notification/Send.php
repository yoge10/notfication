<?php
namespace Ramji\PushNotification\Controller\Adminhtml\Notification;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;


class Send extends Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    protected $_sendNotification;

    /**
     * @var \Magento\Backend\Model\View\Result\Page
     */
    protected $resultPage;
    protected $_messageManager;
    protected $_helperData;
    protected $_notificationCollection;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Ramji\PushNotification\Model\Adminhtml\SendNotification\Notification $sendNotification,
        \Ramji\PushNotification\Helper\Data $helperData,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Ramji\PushNotification\Model\NotificationFactory $notification
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->_sendNotification = $sendNotification;
        $this->_helperData = $helperData;
        $this->_messageManager = $messageManager;
        $this->_notificationCollection = $notification;
    }

    public function execute()
    {
        $triggerType = 2;
        $this->resultPage = $this->resultPageFactory->create();
        $notificationId = $this->getRequest()->getParam('id');
        $checkCronschedule = $this->_helperData->getCronSchedule($notificationId);
        if(empty($notificationId)){
            $notificationData = $this->_helperData->getCronNotifications($triggerType);
            foreach ($notificationData as $collection) {
                $notificationId = $collection->getId();
                $checkCronschedule = $this->_helperData->getCronSchedule($notificationId);
                if($checkCronschedule == 2){
                    $templateInfo = $this->_helperData->getTemplate($notificationId);
                    $notificationSend = $this->_sendNotification->sendNotification($templateInfo,$notificationId);
                    if($notificationSend){
                        $this->_helperData->updateCronTime($notificationId); 
                    }
                }else{
                    $notificationSend = false;
                }
            }
        }elseif($checkCronschedule == 1 && $notificationId){
            $templateInfo = $this->_helperData->getTemplate($notificationId);
            $notificationSend = $this->_sendNotification->sendNotification($templateInfo,$notificationId);
            if($notificationSend){
                $this->_helperData->updateCronTime($notificationId); 
            }
        }else{
            $notificationSend = false;
        }
        if($notificationSend){
            $this->_messageManager->addSuccessMessage('Notification Has Been Sent Successfully');
        }else{
            $this->_messageManager->addError(__("Something went wrong. Please Make Sure The Module is Enabled! or Its on Manual Trigger"));
        }
        $this->_redirect('pushnotification/index/index');
    }
}
