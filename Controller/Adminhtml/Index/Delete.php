<?php
namespace Ramji\PushNotification\Controller\Adminhtml\Index;

class Delete extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        try {
            $notification = $this->_objectManager->get('Ramji\PushNotification\Model\Notification')->load($id);
            $notification->delete();
            $this->messageManager->addSuccess(
                __('Deleted successfully !')
            );
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
	    $this->_redirect('*/*/');
    }
}
