<?php
namespace Ramji\PushNotification\Controller\Adminhtml\Index;
use Magento\Framework\App\Filesystem\DirectoryList;
class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
	public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        if ($data) {
        $model = $this->_objectManager->create('Ramji\PushNotification\Model\Notification');
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            $model->load($id);
        }
        $data['store_view'] = implode(",",$data['store_view']);

        $model->setData($data);

        try {
            $model->save();
            $this->messageManager->addSuccess(__('Notification Has been Saved.'));
            $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
            if ($this->getRequest()->getParam('back')) {
                $this->_redirect('*/*/edit', array('id' => $model->getId(), '_current' => true));
                return;
            }
            $this->_redirect('*/*/');
            return;
        } catch (\Magento\Framework\Model\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\RuntimeException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('Something went wrong while saving the Notification.'.$e->getMessage()));
        }

        $this->_getSession()->setFormData($data);
        $this->_redirect('*/*/edit', array('notification_id' => $this->getRequest()->getParam('notification_id')));
        return;
        }
        $this->_redirect('*/*/');
    }
}
