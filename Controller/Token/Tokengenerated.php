<?php
/**
 *
 * Copyright © 2015 Excellen. All rights reserved.
 */
namespace Ramji\PushNotification\Controller\Token;

class Tokengenerated extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultJsonFactory;
    protected $_token;
    protected $_helperCall;
    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Ramji\PushNotification\Model\SubscriberFactory $tokenFactory,
        \Ramji\PushNotification\Helper\Data $helperCall
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_token = $tokenFactory;
        $this->_helperCall = $helperCall;
    }
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        $tokenVal = $this->getRequest()->getPostValue();
        $tokenData = $this->_token->create();
        $storeId = $this->_helperCall->getStoreId();
        if(!empty($tokenVal)){
            //Fetching Current Browser User is Using.
            $arr_browsers = ["Firefox", "Chrome", "Safari", "Opera", "MSIE", "Trident", "Edge"]; 
            $agent = $_SERVER['HTTP_USER_AGENT'];
            $user_browser = ''; 
            foreach ($arr_browsers as $browser) {
                if (strpos($agent, $browser) !== false) {
                    $user_browser = $browser;
                    break;
                }   
            }
            switch ($user_browser) {
                case 'MSIE':
                    $user_browser = 'Internet Explorer';
                    break;
            
                case 'Trident':
                    $user_browser = 'Internet Explorer';
                    break;
            
                case 'Edge':
                    $user_browser = 'Internet Explorer';
                    break;
            }
            
            $token['token'] = $tokenVal['token'];
            $token['device_type'] = $tokenVal['deviceDetail'];
            $token['browser_name'] = $user_browser;
            $token['store_view'] = $storeId;
            $token['status'] = '1';
            $tokenData->setData($token);
            $tokenSave = $tokenData->save();
            if($tokenSave){
                return $result->setData('true');
            }
        }
    }
}
