<?php
/**
 * Copyright © 2015 Ramji . All rights reserved.
 */
namespace Ramji\PushNotification\Helper;

use Magento\Store\Model\ScopeInterface;
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
	const XML_PATH_PUSHNOTIFICATION = 'Ramji/';
	// for Notification Database 
	protected $_notification;
	// for Template Database
	protected $_template;
	protected $_storeManager;
	protected $date;
	protected $_customOrderDetails;
	/**
	* @param \Magento\Framework\App\Helper\Context $context
	*/
	public function __construct(
	\Magento\Framework\App\Helper\Context $context,
	\Ramji\PushNotification\Model\OrderCollectionFactory $customOrderDetails,
	\Ramji\PushNotification\Model\NotificationFactory $notification,
	\Ramji\PushNotification\Model\TemplatesFactory $template,
	\Magento\Store\Model\StoreManagerInterface $storeManager,
	\Magento\Framework\Stdlib\DateTime\DateTime $date
	) {
		parent::__construct($context);
		$this->_customOrderDetails = $customOrderDetails;
		$this->_notification = $notification;
		$this->_template = $template;
		$this->_storeManager = $storeManager;
		$this->date = $date;
	}
	//get Current Data.
	public function getTimeAccordingToTimeZone()
    {
		date_default_timezone_set('Asia/Kolkata');
		$dateTimeAsTimeZone = $this->date->gmtDate();
        return $dateTimeAsTimeZone;
	}
	//get Time Difference to check Cron Run Functionality
	public function runCronCheck($lastCronRunTime){
		$currentTime = $this->getTimeAccordingToTimeZone();
		$diff = abs(strtotime($currentTime) - strtotime($lastCronRunTime));
		$years   = floor($diff / (365*60*60*24)); 
		$months  = floor(($diff - $years * 365*60*60*24) / (30*60*60*24)); 
		$days    = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
		$hours   = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
		$minuts  = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60 + $hours *60 + $days*24*60*60); 
		$seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60)); 
		return $minuts;
	}
	public function getCronSchedule($notificationId){
		$NotificationCollections = $this->_notification->create()->load($notificationId);
		$customTime = $NotificationCollections->getCustomPeriod();
		$scheduleTime = $NotificationCollections->getScheduleEvent();
		$lastCronRunTime = $NotificationCollections->getCronRunTime();
		$lastTriggerType = $NotificationCollections->getTriggerType();
		$timeDifference = $this->runCronCheck($lastCronRunTime);
		if($lastTriggerType == 2 && $scheduleTime == 6 && $customTime  <= $timeDifference ){
			return 2;
		}elseif($lastTriggerType == 2 && $scheduleTime !==6 && $scheduleTime <=$timeDifference ){
			return 2;
		}elseif($lastTriggerType == 1 ){
			return 1;
		}
	}
	public function getMediaUrl(){
		$mediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
		return $mediaUrl;
	}
	public function getBaseUrl(){
		$baseUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
		return $baseUrl;
	}
	// Get Current Store Id...
	public function getStoreId()
    {
        return $this->_storeManager->getStore()->getId();
    }
	public function getTemplateCollection($templateId){
		$templateData = $this->_template->create()->load($templateId);
		$template['subject'] = $templateData->getSubject();
		$template['messageBody'] = $templateData->getBody();
		$template['icon'] = $templateData->getIcon();
		$template['url'] = $templateData->getDestinationUrl();
		return $template;
	}
	public function updateCronTime($id){
		$notification = $this->_notification->create();
		$notification->load($id);
		$updatedCronTime = $this->getTimeAccordingToTimeZone();
		$notification->setData('cron_run_time',$updatedCronTime);
        $notification->save();

	}
	public function getCronNotifications($triggerType){
		$notificationData = $this->_notification->create()->getCollection();
		return $notificationData->addFieldToFilter('trigger_type',$triggerType)->addFieldToFilter('status',1);
	}
	public function getTemplate($id){
		$NotificationData = $this->_notification->create()->load($id);
		$templateId = $NotificationData->getTemplate();
		return $this->getTemplateCollection($templateId);
	}
	// for StoreView Validation...
	public function getStoreViewValidate($storeView,$notificationId){
		$NotificationData = $this->_notification->create()->load($notificationId);
		$templateId = $NotificationData->getStoreView();
		$storeViewCollection = explode(",",$templateId);
		if(in_array($storeView,$storeViewCollection) || in_array('0',$storeViewCollection)){
			return true;
		}
	}
	// for order status change functionality
	public function getCustomToken($orderId,$customerEmail){
		$collection = $this->_customOrderDetails->create()->getCollection();
		$collection->addFieldToFilter('order_increment_id',$orderId)
		->addFieldToFilter('customer_email',$customerEmail);
		return $collection;
	}
	public function getOrderStatusChange($triggerTypeId,$eventType){
		$notificationData = $this->_notification->create()->getCollection();
		$notificationData = $notificationData->addFieldToFilter('trigger_type',$triggerTypeId)
		->addFieldToFilter('event_type',$eventType)->addFieldToFilter('status',1);
		return $this->getLastTableData($notificationData);
	}
	public function getLastTableData($notificationData){
		$lastCollection = array_values($notificationData->getData());//get Last Table if there are multiple table found.
		$lastData = array_pop($lastCollection);
		if(!empty($lastData)){
			return $lastData['template'];
		}return 0;
	}

	public function getTemplateUpdate($templateCollection,$orderIncrementId,$orderId,$orderStatus,$customerName){
		if((strpos($templateCollection['subject'], '[Order Id]') !== false)){
			$templateCollection['subject'] = str_replace("[Order Id]",$orderIncrementId,$templateCollection['subject']);
		}
		if((strpos($templateCollection['messageBody'], '[Order Status]') !== false)){
			$templateCollection['messageBody'] = str_replace("[Order Status]",$orderStatus,$templateCollection['messageBody']);
		}
		if((strpos($templateCollection['subject'], '[Order Status]') !== false)){
			$templateCollection['subject'] = str_replace("[Order Status]",$orderStatus,$templateCollection['subject']);
		}
		if((strpos($templateCollection['messageBody'], '[Order Id]') !== false)){
			$templateCollection['messageBody'] = str_replace("[Order Id]",$orderIncrementId,$templateCollection['messageBody']);
		}
		if((strpos($templateCollection['subject'], '[Customer Name]') !== false)){
			$templateCollection['subject'] = str_replace("[Customer Name]",$customerName,$templateCollection['subject']);
		}
		if((strpos($templateCollection['messageBody'], '[Customer Name]') !== false)){
			$templateCollection['messageBody'] = str_replace("[Customer Name]",$customerName,$templateCollection['messageBody']);
		}
		if((strpos($templateCollection['url'], '[Order Id]') !== false)){
			$templateCollection['url'] = str_replace("[Order Id]",$orderId,$templateCollection['url']);
		}
		return $templateCollection;
	}
	public function getCustomerTemplateUpdate($templateCollection,$customerName){
		if((strpos($templateCollection['subject'], '[Customer Name]') !== false)){
			$templateCollection['subject'] = str_replace("[Customer Name]",$customerName,$templateCollection['subject']);
		}
		if((strpos($templateCollection['messageBody'], '[Customer Name]') !== false)){
			$templateCollection['messageBody'] = str_replace("[Customer Name]",$customerName,$templateCollection['messageBody']);
		}
		return $templateCollection;
	}
	public function getCustomerproductUpdate($templateCollection,$customerName,$product){
		if((strpos($templateCollection['subject'], '[Customer Name]') !== false)){
			$templateCollection['subject'] = str_replace("[Customer Name]",$customerName,$templateCollection['subject']);
		}
		if((strpos($templateCollection['messageBody'], '[Customer Name]') !== false)){
			$templateCollection['messageBody'] = str_replace("[Customer Name]",$customerName,$templateCollection['messageBody']);
		}
		if((strpos($templateCollection['messageBody'], '[Product Name]') !== false)){
			$templateCollection['messageBody'] = str_replace("[Product Name]",$product,$templateCollection['messageBody']);
		}
		if((strpos($templateCollection['subject'], '[Product Name]') !== false)){
			$templateCollection['subject'] = str_replace("[Product Name]",$product,$templateCollection['subject']);
		}
		return $templateCollection;
	}
	/*...................................................*/
	public function getConfigValue($field,$storeId = null){
		return $this->scopeConfig->getValue(
			$field, ScopeInterface::SCOPE_STORE, $storeId
		);
	}
	public function getGeneralConfig($code, $storeId = null)
	{
		return $this->getConfigValue(self::XML_PATH_PUSHNOTIFICATION .'general/'. $code, $storeId);
	}
	public function getTokenConfig($code, $storeId = null)
	{
		return $this->getConfigValue(self::XML_PATH_PUSHNOTIFICATION .'token/'. $code, $storeId);
	}
}